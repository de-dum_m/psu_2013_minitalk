/*
** server.h for includes in /home/de-dum_m/code/B2-systeme_unix/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Mar 10 17:19:00 2014 de-dum_m
** Last update Mon Mar 10 17:29:44 2014 de-dum_m
*/

#ifndef SERVER_H_
# define SERVER_H_

typedef struct	s_server
{
  int		init;
  int		letter;
}		t_server;

#endif /* !SERVER_H_ */
