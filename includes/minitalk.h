/*
** minitalk.h for includes in /home/de-dum_m/code/B2-systeme_unix/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Mar 10 17:03:05 2014 de-dum_m
** Last update Mon Mar 10 17:04:52 2014 de-dum_m
*/

#ifndef MINITALK_H_
# define MINITALK_H_

# define SUCCESS	1
# define FAILURE	-1

int	my_getnbr(char *str);

#endif /* !MINITALK_H_ */
