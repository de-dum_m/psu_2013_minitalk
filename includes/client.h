/*
** client.h for includes in /home/de-dum_m/code/B2-systeme_unix/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Mar 10 13:21:19 2014 de-dum_m
** Last update Mon Mar 10 17:04:14 2014 de-dum_m
*/

#ifndef CLIENT_H_
# define CLIENT_H_

/*
** send_str.c
*/
int	send_char(char c, int pid);
int	send_str(char *str, int pid);

#endif /* !CLIENT_H_ */
