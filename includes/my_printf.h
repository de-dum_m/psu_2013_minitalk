/*
** my_printf.h for minishell2 in /home/de-dum_m/code/B2-systeme_unix/minishell2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  9 11:13:24 2014 de-dum_m
** Last update Sun Mar  9 11:13:26 2014 de-dum_m
*/

#ifndef MY_PRINTF_H_
# define MY_PRINTF_H_

/*
** my_count_putchar.c
*/
int	count_prints();
void	my_count_putchar(char c, int fd);
void	my_count_putstr(char *str, int fd);

/*
** my_count_put_nbr_base.c
*/
int	my_count_putnbr_base(unsigned int nbr, char *base);
void	chop_chop_count(unsigned int nbr, int b, char *base);

/*
** my_count_put_nbr.c
*/
int	my_count_put_nbr(int nb);

/*
** my_printf.c
*/
int	my_print_error(const char *str, ...);
int	my_printf(const char *str, ...);
char	*my_print_error_chret(const char *str, ...);

/*
** my_printf_tools.c
*/
void	my_put_hex(unsigned int ix, char type);
void	my_put_ptr(unsigned int ptr);
void	my_putstr_weird(char *str, int fd);

/*
** my_put_unsigned_nbr.c
*/
int	my_put_unsigned_nbr(unsigned int nb);
void	print_modified_count(char nb);

#endif /* !MY_PRINTF_H_ */
