##
## Makefile for PSU_2013_minitalk in /home/de-dum_m/code/B2-systeme_unix/PSU_2013_minitalk
## 
## Made by de-dum_m
## Login   <de-dum_m@epitech.net>
## 
## Started on  Mon Mar 10 12:11:21 2014 de-dum_m
## Last update Mon Mar 10 19:08:55 2014 de-dum_m
##

SNAME	= server

CNAME	= client

SSRC	= src/lib/my_getnbr.c \
	src/lib/my_printf/my_count_putchar.c \
	src/lib/my_printf/my_count_put_nbr_base.c \
	src/lib/my_printf/my_count_put_nbr.c \
	src/lib/my_printf/my_printf.c \
	src/lib/my_printf/my_printf_tools.c \
	src/lib/my_printf/my_put_unsigned_nbr.c \
	src/server/server.c

CSRC	= src/client/client_main.c \
	src/client/send_str.c \
	src/lib/my_getnbr.c \
	src/lib/my_printf/my_count_putchar.c \
	src/lib/my_printf/my_count_put_nbr_base.c \
	src/lib/my_printf/my_count_put_nbr.c \
	src/lib/my_printf/my_printf.c \
	src/lib/my_printf/my_printf_tools.c \
	src/lib/my_printf/my_put_unsigned_nbr.c


SOBJ	= $(SSRC:.c=.o)

COBJ	= $(CSRC:.c=.o)

CFLAGS	= -Wall -Wextra -W -pedantic -Iincludes -g

all:	$(CNAME) $(SNAME)

$(CNAME): $(COBJ)
	$(CC) $(COBJ) -o $(CNAME)
	@echo -e "[032mClient compiled successfully[0m"

$(SNAME): $(SOBJ)
	$(CC) $(SOBJ) -o $(SNAME)
	@echo -e "[032mServer compiled successfully[0m"

clean:
	rm -f $(COBJ)

fclean:	clean
	rm -f $(CNAME)

re:	fclean all

.PHONY:	all re fclean clean
