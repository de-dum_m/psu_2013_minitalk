/*
** server.c for server in /home/de-dum_m/code/B2-systeme_unix/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Mar 10 16:49:01 2014 de-dum_m
** Last update Mon Mar 10 20:31:06 2014 de-dum_m
*/

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include "server.h"
#include "minitalk.h"
#include "my_printf.h"

static t_server	g_serv;

static void	catch_sigusr2(int nb)
{
  (void)nb;
  if (g_serv.init != 0)
    my_printf("\033[31m");
  g_serv.letter++;
}

static void	catch_sigusr1(int nb)
{
  char		letter;

  (void)nb;
  g_serv.init++;
  if (g_serv.init == 1 && g_serv.letter > 0)
    {
      letter = g_serv.letter;
      write(1, &letter, 1);
      g_serv.init = 0;
      g_serv.letter = 0;
    }
  else
    {
      g_serv.letter = -1;
      g_serv.init = 0;
      my_printf("\033[0m");
      my_printf("\n");
    }
}

static int	wait_for_client()
{
  if (signal(SIGUSR1, &catch_sigusr1) == SIG_ERR)
    return (FAILURE);
  if (signal(SIGUSR2, &catch_sigusr2) == SIG_ERR)
    return (FAILURE);
  while (42)
    {
      pause();
    }
}

int	main()
{
  pid_t	my_pid;

  my_pid = getpid();
  my_printf("%d\n", my_pid);
  g_serv.init = 0;
  g_serv.letter = -1;
  wait_for_client();
  return (SUCCESS);
}
