/*
** my_getnbr.c for minishell2 in /home/de-dum_m/code/B2-systeme_unix/minishell2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  9 17:00:07 2014 de-dum_m
** Last update Sun Mar  9 17:00:08 2014 de-dum_m
*/

int	my_getnbr(char *str)
{
  int	i;
  int	res;

  i = 0;
  res = 0;
  while (str[i])
    {
      if (str[i] >= '0' && str[i] <= '9')
	res = (res * 10) + (str[i] - 48);
      else if (str[i] != '-')
	return (res);
      i++;
    }
  if (str[0] == '-')
    res = - res;
  return (res);
}
