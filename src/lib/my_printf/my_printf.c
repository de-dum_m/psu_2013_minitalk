/*
** my_printf.c for minishell2 in /home/de-dum_m/code/B2-systeme_unix/minishell2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  9 17:00:42 2014 de-dum_m
** Last update Sun Mar  9 17:01:00 2014 de-dum_m
*/

#include <stdarg.h>
#include <stdio.h>
#include "my_printf.h"

static int   	det_type(va_list ap, char type, int fd)
{
  if (type == 'd' || type == 'i')
    my_count_put_nbr(va_arg(ap, int));
  else if (type == 's')
    my_count_putstr(va_arg(ap, char*), fd);
  else if (type == 'p')
    my_put_ptr(va_arg(ap, unsigned int));
  else if (type == 'u')
    my_put_unsigned_nbr(va_arg(ap, unsigned int));
  else if (type == 'c' || type == 'C')
    my_count_putchar(va_arg(ap, int), fd);
  else if (type == 'x' || type == 'X')
    my_put_hex(va_arg(ap, unsigned int), type);
  else if (type == 'b')
    my_count_putnbr_base(va_arg(ap, unsigned int), "01");
  else if (type == 'S')
    my_putstr_weird(va_arg(ap, char*), fd);
  else if (type == 'o')
    my_count_putnbr_base(va_arg(ap, unsigned int), "01234567");
  else
    return (0);
  return (1);
}

static int	my_dual_printf(const char *str, int fd, va_list ap)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] == '%' && str[i + 1] != '%')
	{
	  if (det_type(ap, str[i + 1], fd) == 0)
	    return (0);
	  i = i + 1;
	}
      else if (str[i] == '%')
	{
	  my_count_putchar('%', fd);
	  i = i + 1;
	}
      else
	my_count_putchar(str[i], fd);
      i = i + 1;
    }
  va_end(ap);
  return (count_prints() - 1);
}

int		my_print_error(const char *str, ...)
{
  va_list	ap;

  va_start(ap, str);
  my_dual_printf(str, 2, ap);
  return (0);
}

char		*my_print_error_chret(const char *str, ...)
{
  va_list	ap;

  va_start(ap, str);
  my_dual_printf(str, 2, ap);
  return (NULL);
}

int		my_printf(const char *str, ...)
{
  va_list	ap;

  va_start(ap, str);
  return (my_dual_printf(str, 1, ap));
}
