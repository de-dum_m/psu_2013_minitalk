/*
** my_count_putchar.c for minishell2 in /home/de-dum_m/code/B2-systeme_unix/minishell2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  9 11:19:39 2014 de-dum_m
** Last update Sun Mar  9 17:00:14 2014 de-dum_m
*/

#include <unistd.h>

int		count_prints()
{
  static int	prints;

  prints = prints + 1;
  return (prints);
}

void	my_count_putchar(char c, int fd)
{
  if (!c)
    return ;
  else
    write(fd, &c, 1);
  count_prints();
}

void	my_count_putstr(char *str, int fd)
{
  int	i;

  i = 0;
  while (str && str[i])
    {
      my_count_putchar(str[i], fd);
      i = i + 1;
    }
}
