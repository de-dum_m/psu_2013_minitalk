/*
** my_put_unsigned_nbr.c for minishell2 in /home/de-dum_m/code/B2-systeme_unix/minishell2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  9 17:00:35 2014 de-dum_m
** Last update Sun Mar  9 17:00:35 2014 de-dum_m
*/

#include "my_printf.h"

void	print_modified_count(char nb)
{
  my_count_putchar(nb + 48, 1);
}

int	my_put_unsigned_nbr(unsigned int nb)
{
  if (nb >= 10)
    {
      my_count_put_nbr(nb / 10);
      my_count_put_nbr(nb % 10);
    }
  else
    print_modified_count(nb);
  return (0);
}
