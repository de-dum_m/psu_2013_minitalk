/*
** my_count_put_nbr_base.c for minishell2 in /home/de-dum_m/code/B2-systeme_unix/minishell2
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Sun Mar  9 16:47:06 2014 de-dum_m
** Last update Sun Mar  9 16:47:07 2014 de-dum_m
*/

#include "my_printf.h"

void	chop_chop_count(unsigned int nbr, int b, char *base)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  if (nbr / b)
    {
      chop_chop_count(nbr / b, b, base);
      chop_chop_count(nbr % b, b, base);
    }
  if (nbr / b  == 0)
    {
      while ((unsigned int)i < nbr)
	{
	  i = i + 1;
	  j = j + 1;
	  if (i == b)
	    j = 0;
	}
      my_count_putchar(base[j], 1);
    }
}

int	my_count_putnbr_base(unsigned int nbr, char *base)
{
  int	b;

  if (nbr > nbr + 1 || nbr + 100 < 100)
    return (nbr);
  b = 0;
  if (base[0] == '\0')
    return (0);
  while (base[b] != '\0')
    b = b + 1;
  chop_chop_count(nbr, b, base);
  return (nbr);
}
