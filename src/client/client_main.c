/*
** client_main.c for client in /home/de-dum_m/code/B2-systeme_unix/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Mar 10 12:30:27 2014 de-dum_m
** Last update Mon Mar 10 17:04:07 2014 de-dum_m
*/

#include "client.h"
#include "minitalk.h"
#include "my_printf.h"

static int	usage()
{
  my_printf("Usage: client PID STRING\n");
  return (FAILURE);
}

int	main(int ac, char **av)
{
  if (ac != 3)
    return (usage());
  else if (my_getnbr(av[1]) > 0)
    {
      if (send_str(av[2], my_getnbr(av[1])) == FAILURE)
	{
	  my_print_error("Signaling server failed, please forgive me...\n");
	  return (FAILURE);
	}
    }
  else
    return (usage());
  return (SUCCESS);
}
