/*
** send_str.c for client in /home/de-dum_m/code/B2-systeme_unix/PSU_2013_minitalk
**
** Made by de-dum_m
** Login   <de-dum_m@epitech.net>
**
** Started on  Mon Mar 10 15:54:03 2014 de-dum_m
** Last update Mon Mar 10 20:38:10 2014 de-dum_m
*/

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include "client.h"
#include "minitalk.h"

int	send_char(char c, int pid)
{
  int	i;

  i = 0;
  while (i < c)
    {
      if (kill(pid, SIGUSR2) == -1)
	return (FAILURE);
      i++;
      usleep(20);
    }
  usleep(50);
  return (SUCCESS);
}

int	send_str(char *str, int pid)
{
  int	i;

  i = 0;
  while (str && str[i])
    {
      if (send_char(str[i], pid) == FAILURE)
	return (FAILURE);
      if (str[i + 1] && kill(pid, SIGUSR1) == -1)
	return (FAILURE);
      i++;
    }
  usleep(10);
  if (kill(pid, SIGUSR2) == -1
      || kill(pid, SIGUSR1) == -1
      || kill(pid, SIGUSR1) == -1)
    return (FAILURE);
  usleep(10000);
  if (kill(pid, SIGUSR1) == -1)
    return (FAILURE);
  return (SUCCESS);
}
